import sys
from PyQt5 import QtGui, QtCore
from PyQt5 import QtQml

import os
os.chdir('/opt/Qt/Examples/Qt-5.15.0/location/mapviewer')
#os.chdir('/home/pi/Documents/reto_hundert_automatizacion/hud/mapviewer')
os.environ['QT_PLUGIN_PATH'] = "/home/pi/Qt/5.15.0/gcc_64/plugins"
os.environ['QML_IMPORT_TRACE'] = "1"
os.environ['OPENSSL_LIBS']="/usr/lib"
os.environ['QML2_IMPORT_PATH'] = "/home/pi/Qt/5.15.0/gcc_64/qml"

app = QtGui.QGuiApplication(sys.argv)
engine = QtQml.QQmlApplicationEngine()
engine.load(QtCore.QUrl('./mapviewer.qml'))
app.exec_()