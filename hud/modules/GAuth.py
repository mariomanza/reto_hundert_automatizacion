# Import basics
import sys
import os
import json

# Module for easy OAuth2 usage, based on the requests library,
# which is the easiest way to perform HTTP requests.

# OAuth2Session object
from requests_oauthlib import OAuth2Session
# Mobile application client that does not need a client_secret
from oauthlib.oauth2 import MobileApplicationClient

config = json.loads(open('conifg/gauth.json'))

print(config)
#%%----------- Main configuration settings ----------------
client_id = "cbc4c47b711a4feab974223b255c81c1"
# TESTED, just redirecting to Google works in normal browsers
# the token string appears in the url of the address bar
redirect_uri = "https://google.nl"

# Generate correct URLs
base_url = "https://test-accounts.osf.io/oauth2/"
auth_url = base_url + "authorize"
token_url = base_url + "token"
#%%--------------------------------------------------------

mobile_app_client = MobileApplicationClient(client_id)

# Create an OAuth2 session for the OSF
osf_auth = OAuth2Session(
    client_id, 
    mobile_app_client,
    scope="osf.full_write", 
    redirect_uri=redirect_uri,
)

def get_authorization_url():
    """ Generate the URL with which one can authenticate at the OSF and allow 
    OpenSesame access to his or her account."""
    return osf_auth.authorization_url(auth_url)

def parse_token_from_url(url):
    token = osf_auth.token_from_fragment(url)
    if token:
        return token
    else:
        return osf_auth.fetch_token(url)