#Interface sizing functions
def c(a,b,c):
    return int(c*b/a)

def p(screen, size):
    new_size = []
    screen = screen+screen
    for i, e in enumerate(size):
        o = 1280 if i%2==0 else 720
        n = 0 if e == 0 else c(o, e, screen[i])
        new_size.append(n)
    return new_size