import os, sys, subprocess, serial, serial.tools.list_ports
import time
from datetime import datetime
from PyQt5 import QtWidgets, uic, QtGui, QtCore#, QtWebEngineWidgets
from PyQt5.QtQml import QQmlComponent, QQmlEngine
from modules.AppBar import Ui_AppBar
from modules.Utils import *
from modules.MediaPlayer import MediaPlayer

#from pyqtlet import L, MapWidget

import random

def c(a,b,c):
    return int(c*b/a)

def p(screen, size):
    new_size = []
    screen = screen+screen
    for i, e in enumerate(size):
        o = 1280 if i%2==0 else 720
        n = 0 if e == 0 else c(o, e, screen[i])
        new_size.append(n)
    return new_size

#main class
class Hud(QtWidgets.QMainWindow, Ui_AppBar, MediaPlayer):
    def __init__(self, screen, *args, obj=None, **kwargs):
        super(Hud, self).__init__(*args, **kwargs)
        self.width = screen[0]
        self.height = screen[1]

        self.setupHomeUi(self)
        self.setupUi(self)
        self.setupMediaPlayer()
        self.set_img('intro_logo', 'logo.png')

        self.anim = QtCore.QPropertyAnimation(self.intro_logo, b"geometry")
        self.anim.setDuration(1000)
        self.anim.setLoopCount(3)
        self.anim.setStartValue(QtCore.QRect(*p([self.width, self.height],[492, 135, 296, 450])))
        self.anim.setKeyValueAt(.5,QtCore.QRect(*p([self.width, self.height],[640, 135, 0, 450])))
        self.anim.setEndValue(QtCore.QRect(*p([self.width, self.height],[492, 135, 296, 450])))
        self.anim.start()
        QtCore.QTimer.singleShot(4000, self.clearintro)

    def conn_arduino(self):
        self.arduino_flag = False
        for p in list(serial.tools.list_ports.comports()):
            self.arduino = serial.Serial(p[0], 115200, timeout=.1)
            if self.arduino.is_open:
                self.arduino.write(b'1')
                self.arduino_flag = True
                print("Connected "+p.serial_number)

    def clearintro(self):
        self.conn_arduino()
        self.intro_back.setParent(None)
        self.intro_logo.setParent(None)
        self.startMenus()
        self.startHomeUi()
    
    def startMenus(self):
        #ac menu
        self.ac_icons = {'flow_top': [0,'icons/flow_top.png'], 'flow_bottom': [0,'icons/flow_bottom.png'],
        'pilot_temp_up': [0,'icons/up.png'], 'pilot_temp_down': [0,'icons/down.png'], 'copilot_temp_up': [0,'icons/up.png'],
        'copilot_temp_down': [0,'icons/down.png'], 'fan_bttn': [0,'icons/fan.svg'], 'defog': [0,'icons/defog.png'],
        'recirculate': [0,'icons/recirculate.png']}

        for name, data in self.ac_icons.items():
            self.set_icon(name, data[1])

        self.ac_temp_copilot.setText("<html><head/><body><p align=\"center\"><span style=\" font-size:"+str(c(1280, 42, self.width))+"pt; color:#d3d7cf;\">26°</span></p></body></html>")
        self.ac_temp_pilot.setText("<html><head/><body><p align=\"center\"><span style=\" font-size:"+str(c(1280, 42, self.width))+"pt; color:#d3d7cf;\">26°</span></p></body></html>")
        
        #browser
        #self.browser = QtWebEngineWidgets.QWebEngineView(self)
        #self.browser.setGeometry(QtCore.QRect(*p([self.width, self.height],[420, 90, 860, 630])))
        #map
        """self.qml_engine = QQmlEngine(self)
        self.map_component = QQmlComponent(self.qml_engine, QtCore.QUrl("/home/pi/Documents/reto_hundert_automatizacion/hud/mapviewer/mapviewer.qml"), self)
        self.map = self.map_component.create()"""

        self.mapWidget = MapWidget()
        self.map = L.map(self.mapWidget)
        self.mapWidget.setGeometry(QtCore.QRect(*p([self.width, self.height],[0, 0, 860, 630])))

        self.map = L.map(self.mapWidget)
        self.map.setView([12.97, 77.59], 10)
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(self.map)
        self.marker = L.marker([12.934056, 77.610029])
        self.marker.bindPopup('Maps are a treasure.')
        self.map.addLayer(self.marker)

        self.map_frame = QtWidgets.QWidget(self)
        self.map_frame.setGeometry(QtCore.QRect(*p([self.width, self.height],[420, 90, 860, 630])))
        self.map_layout = QtWidgets.QVBoxLayout(self.map_frame)
        self.map_layout.addWidget(self.mapWidget)

        self.map_frame.show()
        self.map_frame.raise_()

    def startHomeUi(self):
        self.timer = QtCore.QTimer()
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.get_time)
        self.timer.timeout.connect(self.get_battery)
        self.timer.start()

        self.buttons = {'light':[0,'icons/light.svg'],'in_light':[0,'icons/in_light.svg'],'wipers':[0,'icons/wipers.svg'],
        'fan':[0,'icons/fan.svg'],'temp_up':[0,'icons/up.png'],'temp_down':[0,'icons/down.png'],
        'config':[0,'icons/config.svg'],'spotify':[0,'icons/spotify.svg'],'ac':[0,'icons/ac.svg'],
        'nav':[0,'icons/nav.svg'],'stats':[0,'icons/stats.svg'],'home':[0,'icons/home.svg']}

        for name, data in self.buttons.items():
            self.set_icon(name, data[1])
        
        #button connection
        if self.arduino_flag:
            self.l = 1
            self.light.clicked.connect(self.lights_on_off)
            self.t = 26
            self.temp_up.clicked.connect(lambda: self.set_temp(1))
            self.temp_down.clicked.connect(lambda: self.set_temp(-1))

        #navbar connection
        self.nav.clicked.connect(self.open_maps)
        self.spotify.clicked.connect(self.open_spotify)
        self.ac.clicked.connect(self.open_ac)
        self.deezer.clicked.connect(self.open_dz)
        self.stats.clicked.connect(self.open_yt)

        self.doors = {'pilot':[0, 'model/pilot_open.png'],'copilot':[0,'model/copilot_open.png'],
        'slide_l':[0,'model/slide_l_open.png'],'slide_r':[0,'model/slide_r_open.png'],'trunk':[0,'model/trunk_open.png']}

        for name, data in self.doors.items():
            self.set_icon(name, 'open.png', "border: none;")
        
        #door connection
        self.pilot.clicked.connect(lambda: self.door('pilot', 'p'))
        self.copilot.clicked.connect(lambda: self.door('copilot', 'c'))
        self.slide_l.clicked.connect(lambda: self.door('slide_l', 'l'))
        self.slide_r.clicked.connect(lambda: self.door('slide_r', 'r'))
        self.trunk.clicked.connect(lambda: self.door('trunk', 't'))
        
        self.set_img('model', 'model/all_closed.png')
        self.set_img('logo', 'logo.png')

    def open_ac(self):
        self.ac_menu.show()
        self.ac_menu.raise_()
    
    def open_maps(self):
        self.map_frame.show()
        self.map_frame.raise_()
    
    def open_yt(self):
        #self.browser.setUrl(QtCore.QUrl("https://music.youtube.com/"))
        #self.browser.show()
        #self.browser.raise_()
        pass

    def open_dz(self):
        #self.browser.setUrl(QtCore.QUrl("https://www.deezer.com/mx/"))#"https://open.spotify.com/"))#"https://music.youtube.com/"))
        #self.browser.show()
        #self.browser.raise_()
        pass

        #self.browser.loadFinished.connect(self.onLoadFinished)

    def onLoadFinished(self):
        banner = ".conversion-banner"
        shows = "li.sidebar-nav-item:nth-child(2)"
        left_menu = "#page_sidebar"
        top_bar = "#page_topbar"
        carrousel = "section.channel-section:nth-child(6)" #17

        js = "document.querySelector('"+shows+"').remove();document.querySelector('"+top_bar+"').remove();document.querySelector('"+banner+"').remove();"
        js += "".join("document.querySelector('section.channel-section:nth-child("+x+")').remove();" for x in range(6,18))
        js = js.split(';')
        #for el in js:
            #self.browser.page().runJavaScript(el+";")

        """if self.map.isReady():
            print("map is ready")
            self.map_frame = QtWidgets.QWidget(self)
            self.map_frame.setGeometry(QtCore.QRect(*p([self.width, self.height],[420, 90, 860, 630])))
            self.map_layout = QtWidgets.QVBoxLayout(self.map_frame)
            self.map_layout.addWidget(self.map)

            self.map_frame.show()
            self.map_frame.raise_()"""

    def open_spotify(self):
        self.centralWidget.show()
        self.centralWidget.raise_()

    def lights_on_off(self):
        c = b'F' if self.l == 1 else b'F'
        QtCore.QTimer.singleShot(100, lambda: self.arduino.write(c))
        self.l = 0 if self.l == 1 else 1
    
    def set_temp(self, d=1):
        self.t += d
        self.temp.setText("<html><head/><body><p align=\"center\"><span style=\" font-size:"+str(c(1280, 18, self.width))+"pt; color:#d3d7cf;\">"+self.t+"°</span></p></body></html>")

    def set_icon(self, name, icon, style="background-color: rgb(88,88,88);"):
        s = "resources/"+icon
        if ".svg" in icon:
            icon = s
        else:
            icon = QtGui.QPixmap(s)
        getattr(self,name).setIcon(QtGui.QIcon(icon))
        getattr(self,name).setIconSize(getattr(self,name).size())
        getattr(self,name).setStyleSheet(style)
    
    def set_img(self, name, img):
        getattr(self,name).setScaledContents(True)
        getattr(self,name).setPixmap(QtGui.QPixmap("/home/pi/Documents/reto_hundert_automatizacion/hud/resources/"+img))

    def door(self, obj, p):
        num = self.doors[obj][0]
        #arduino signal
        if self.arduino_flag:
            c = p.upper().encode('utf-8') if num == 1 else p.lower().encode('utf-8')
            QtCore.QTimer.singleShot(100, lambda: self.arduino.write(c))
        #button icon
        name = 'open.png' if num == 1 else 'close.png'
        self.set_icon(obj, name, "border: none;")
        #model img
        img = '' if num == 1 else self.doors[obj][1]
        self.set_img("model_"+obj, img)
        self.doors[obj][0] = 0 if num == 1 else 1
    
    def get_time(self):
        h,m,p = datetime.now().strftime("%I %M %p").split()
        self.clock.setText("<html><head/><body><p align=\"center\"><span style=\" font-size:"+str(c(1280, 18, self.width))+"pt; color:#d3d7cf;\">"+h+":"+m+" </span><span style=\" font-size:"+str(c(1280, 18, self.width))+"pt; font-weight:600; color:#d3d7cf;\">"+p+"</span></p></body></html>")
    
    def get_battery(self):
        self.battery.setValue(random.randint(0, 100))
        self.range.setText("<html><head/><body><p align=\"center\"><span style=\" font-size:"+str(c(1280, 12, self.width))+"pt; color:#d3d7cf;\">Alcance</span></p><p align=\"center\"><span style=\" font-size:"+str(c(1280, 14, self.width))+"pt; font-weight:600; color:#d3d7cf;\">150 Km</span></p></body></html>")

if __name__ == "__main__":
    import os
    os.chdir('/home/pi/Documents/reto_hundert_automatizacion_automatizacion/hud/')
    app = QtWidgets.QApplication(sys.argv)
    res = app.desktop().screenGeometry()
    screen = [res.width(),res.height()]
    #screen = [1280,720]
    window = Hud(screen)
    window.resize(*screen)
    window.showFullScreen()
    app.exec()
