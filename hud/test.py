# Oauth2 connection to OSF
# Import basics
import sys
import os
import json

# Module for easy OAuth2 usage, based on the requests library,
# which is the easiest way to perform HTTP requests.

# OAuth2Session object
from requests_oauthlib import OAuth2Session
# Mobile application client that does not need a client_secret
from oauthlib.oauth2 import MobileApplicationClient

config = json.loads(open(os.getcwd()+'/hud/modules/config/gauth.json').read())

#%%----------- Main configuration settings ----------------
client_id = config['client_id']
# TESTED, just redirecting to Google works in normal browsers
# the token string appears in the url of the address bar
redirect_uri = config['redirect_uris'][0]

# Generate correct URLs
auth_url = config['auth_uri']
token_url = config['token_uri']
#%%--------------------------------------------------------

mobile_app_client = MobileApplicationClient(client_id)

# Create an OAuth2 session for the OSF
osf_auth = OAuth2Session(
    client_id, 
    mobile_app_client,
    scope="full_write", 
    redirect_uri=redirect_uri,
)

def get_authorization_url():
    """ Generate the URL with which one can authenticate at the OSF and allow 
    OpenSesame access to his or her account."""
    return osf_auth.authorization_url(auth_url)

def parse_token_from_url(url):
    token = osf_auth.token_from_fragment(url)
    if token:
        return token
    else:
        return osf_auth.fetch_token(url)

import sys
from PyQt5 import QtWidgets, QtCore, QtWebEngineWidgets, QtNetwork

class LoginWindow(QtWebEngineWidgets.QWebEngineView):
    """ A Login window for the OSF """
    # Login event is emitted after successfull login
    logged_in = QtCore.pyqtSignal(['QString'])  

    def __init__(self):
        super(LoginWindow, self).__init__()

        # Create Network Access Manager to listen to all outgoing
        # HTTP requests. Necessary to work around the WebKit 'bug' which
        # causes it drop url fragments, and thus the access_token that the
        # OSF Oauth system returns
        self.nam = QtNetwork.QNetworkAccessManager()

        # Connect event that is fired if a HTTP request is completed.
        self.nam.finished.connect(self.checkResponse)

    def checkResponse(self,reply):
        request = reply.request()
        # Get the HTTP statuscode for this response
        statuscode = reply.attribute(request.HttpStatusCodeAttribute)
        # The accesstoken is given with a 302 statuscode to redirect

        if statuscode == 302:
            redirectUrl = reply.attribute(request.RedirectionTargetAttribute)
            if redirectUrl.hasFragment():
                r_url = redirectUrl.toString()
                if osf.redirect_uri in r_url:
                    print("Token URL: {}".format(r_url))
                    self.token = osf.parse_token_from_url(r_url)
                    if self.token:
                        self.logged_in.emit("login")
                        self.close()

if __name__ == "__main__":
    """ Test if user can connect to OSF. Opens up a browser window in the form
    of a QWebView window to do so."""
    # Import QT libraries

    app = QtWidgets.QApplication(sys.argv)
    browser = LoginWindow()

    auth_url, state = osf.get_authorization_url()
    print("Generated authorization url: {}".format(auth_url))

    browser_url = QtCore.QUrl.fromEncoded(auth_url)
    browser.load(browser_url)
    browser.set_state(state)
    browser.show()

    exitcode = app.exec_()
    print("App exiting with code {}".format(exitcode))
    sys.exit(exitcode)