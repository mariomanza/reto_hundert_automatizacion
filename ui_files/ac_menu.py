# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ac_menu.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_AppBar(object):
    def setupUi(self, AppBar):
        AppBar.setObjectName("AppBar")
        AppBar.resize(860, 630)
        AppBar.setStyleSheet("")
        self.widget = QtWidgets.QWidget(AppBar)
        self.widget.setGeometry(QtCore.QRect(0, 0, 860, 630))
        self.widget.setStyleSheet("background-color: rgb(136, 138, 133);")
        self.widget.setObjectName("widget")
        self.flow_top = QtWidgets.QPushButton(self.widget)
        self.flow_top.setGeometry(QtCore.QRect(330, 90, 200, 150))
        self.flow_top.setStyleSheet("background-color: rgb(78, 154, 6);")
        self.flow_top.setText("")
        self.flow_top.setObjectName("flow_top")
        self.flow_bottom = QtWidgets.QPushButton(self.widget)
        self.flow_bottom.setGeometry(QtCore.QRect(330, 240, 200, 150))
        self.flow_bottom.setStyleSheet("background-color: rgb(78, 154, 6);")
        self.flow_bottom.setText("")
        self.flow_bottom.setObjectName("flow_bottom")
        self.temp_up_pilot = QtWidgets.QPushButton(self.widget)
        self.temp_up_pilot.setGeometry(QtCore.QRect(60, 150, 200, 60))
        self.temp_up_pilot.setStyleSheet("background-color: rgb(32, 74, 135);")
        self.temp_up_pilot.setText("")
        self.temp_up_pilot.setObjectName("temp_up_pilot")
        self.temp_down_pilot = QtWidgets.QPushButton(self.widget)
        self.temp_down_pilot.setGeometry(QtCore.QRect(60, 310, 200, 60))
        self.temp_down_pilot.setStyleSheet("background-color: rgb(32, 74, 135);")
        self.temp_down_pilot.setText("")
        self.temp_down_pilot.setObjectName("temp_down_pilot")
        self.copilot_temp_up = QtWidgets.QPushButton(self.widget)
        self.copilot_temp_up.setGeometry(QtCore.QRect(600, 150, 200, 60))
        self.copilot_temp_up.setStyleSheet("background-color: rgb(32, 74, 135);")
        self.copilot_temp_up.setText("")
        self.copilot_temp_up.setObjectName("copilot_temp_up")
        self.copilot_temp_down = QtWidgets.QPushButton(self.widget)
        self.copilot_temp_down.setGeometry(QtCore.QRect(600, 310, 200, 60))
        self.copilot_temp_down.setStyleSheet("background-color: rgb(32, 74, 135);")
        self.copilot_temp_down.setText("")
        self.copilot_temp_down.setObjectName("copilot_temp_down")
        self.fan_bttn = QtWidgets.QPushButton(self.widget)
        self.fan_bttn.setGeometry(QtCore.QRect(340, 470, 180, 60))
        self.fan_bttn.setStyleSheet("background-color: rgb(52, 101, 164);")
        self.fan_bttn.setText("")
        self.fan_bttn.setObjectName("fan_bttn")
        self.fan_speed = QtWidgets.QLabel(self.widget)
        self.fan_speed.setGeometry(QtCore.QRect(310, 410, 240, 50))
        self.fan_speed.setStyleSheet("background-color: rgb(204, 0, 0);")
        self.fan_speed.setText("")
        self.fan_speed.setObjectName("fan_speed")
        self.ac_temp_copilot = QtWidgets.QLabel(self.widget)
        self.ac_temp_copilot.setGeometry(QtCore.QRect(600, 210, 200, 100))
        self.ac_temp_copilot.setStyleSheet("background-color: rgb(204, 0, 0);")
        self.ac_temp_copilot.setText("")
        self.ac_temp_copilot.setObjectName("ac_temp_copilot")
        self.ac_temp_pilot = QtWidgets.QLabel(self.widget)
        self.ac_temp_pilot.setGeometry(QtCore.QRect(60, 210, 200, 100))
        self.ac_temp_pilot.setStyleSheet("background-color: rgb(204, 0, 0);")
        self.ac_temp_pilot.setText("")
        self.ac_temp_pilot.setObjectName("ac_temp_pilot")
        self.defog = QtWidgets.QPushButton(self.widget)
        self.defog.setGeometry(QtCore.QRect(80, 50, 220, 70))
        self.defog.setText("")
        self.defog.setObjectName("defog")
        self.recirculate = QtWidgets.QPushButton(self.widget)
        self.recirculate.setGeometry(QtCore.QRect(560, 50, 220, 70))
        self.recirculate.setText("")
        self.recirculate.setObjectName("recirculate")
        self.seat_pilot = QtWidgets.QPushButton(self.widget)
        self.seat_pilot.setGeometry(QtCore.QRect(60, 430, 200, 80))
        self.seat_pilot.setText("")
        self.seat_pilot.setObjectName("seat_pilot")
        self.seat_copilot = QtWidgets.QPushButton(self.widget)
        self.seat_copilot.setGeometry(QtCore.QRect(600, 430, 200, 80))
        self.seat_copilot.setText("")
        self.seat_copilot.setObjectName("seat_copilot")

        self.retranslateUi(AppBar)
        QtCore.QMetaObject.connectSlotsByName(AppBar)

    def retranslateUi(self, AppBar):
        _translate = QtCore.QCoreApplication.translate
        AppBar.setWindowTitle(_translate("AppBar", "AppBar"))
