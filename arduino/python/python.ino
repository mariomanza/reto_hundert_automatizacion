//Entradas 
int camara_izquierda = 2;
int camara_derecha= 3;
int camara_atras = 4;
int camara_adelante = 5;

int sensores = 6;
int temperatura = 7;
int velocidad = 8;
int amperaje = 9;
int gps = 10;
int luz = 11;
int microfono = 12;
int palanca_izq = 13;
int palanca_der = 14;
int intermitente = 15;

//Salidas
int puerta_piloto[] = {16, 5};
int puerta_copiloto[] = {17, 5};
int puerta_izquierda[] = {18, 10};
int puerta_derecha[] = {19, 10};
int puerta_trasera[] = {20, 7};
int luz_exterior = 21;
int luz_interior = 22;
int direccional_der = 23;
int direccional_izq = 24;

char data;


void setup() {
  Serial.begin(115200);
  pinMode(camara_izquierda,INPUT); // a A
  pinMode(camara_derecha,INPUT); // b B
  pinMode(camara_izquierda,INPUT);
  pinMode(camara_atras,INPUT);
  pinMode(camara_adelante,INPUT);
  
  pinMode(sensores,INPUT);
  pinMode(temperatura,INPUT);
  pinMode(velocidad,INPUT);
  pinMode(amperaje,INPUT);
  pinMode(gps,INPUT);
  pinMode(microfono,INPUT);
  
  
  pinMode(palanca_der,INPUT);
  pinMode(palanca_izq,INPUT);
  pinMode(intermitente,INPUT);
  
  pinMode(puerta_piloto[0] ,OUTPUT); // l L steps
  pinMode(puerta_copiloto[0] ,OUTPUT); // m M steps
  pinMode(puerta_izquierda[0],OUTPUT); // n N steps
  pinMode(puerta_derecha[0],OUTPUT);
  pinMode(puerta_trasera[0],OUTPUT);
  pinMode(luz_exterior,OUTPUT); // p P on/off
  pinMode(luz_interior,OUTPUT); // q Q on/off
  pinMode(direccional_der,OUTPUT); // input palanza_der alternate on/off
  pinMode(direccional_izq,OUTPUT); // input palanca_izq alternate on/off
  Serial.print("Ready...");
}

void activate_door(int door[], int action) {
  int steps = 0;
  int start = 0;
  if (action > 0) {
    start = 0;
    steps = door[1];
  } else {
    start = door[1];
    steps = 0;
  }
  for (int i=0; i <= steps; i = i+action) {
    digitalWrite(door[0], HIGH);
    delay(1000);
  }
  digitalWrite(door[0], LOW);
}

void blinker(int x, int y = -1) {
  while (true) {
    digitalWrite(x, HIGH);
    digitalWrite(y, HIGH);
    delay(1000);
    digitalWrite(x, LOW);
    digitalWrite(y, LOW);
    delay(1000);
  }
}

//code  will not recieve commands when busy, 
//adding https://create.arduino.cc/projecthub/reanimationxp/how-to-multithread-an-arduino-protothreading-tutorial-dd2c37 fixes that
//will probably need arduino mega or due, too much pins needed, or multiple arduinos
void loop() {
  if (Serial.available() > 0) {
    data = Serial.read();
  }
  switch (data) {
    case 'l':
      activate_door(puerta_piloto, 1);
    break;
    case 'L':
      activate_door(puerta_piloto, -1);
    break;
    case 'p':
      digitalWrite(luz_exterior, HIGH);
    break;
    case 'P':
      digitalWrite(luz_exterior, LOW);
    break;
  }
  if (digitalRead(palanca_izq)) {
    blinker(direccional_izq);
  }
  if (digitalRead(palanca_der)) {
    blinker(direccional_der);
  }
  if (digitalRead(intermitente)) {
    blinker(direccional_der, direccional_izq);
  }
}
